FROM debian:stretch-slim
MAINTAINER "Emmanuel Cohen <atmaniak@gmail.com>"

ARG BUILD_DATE
ARG SOURCE_COMMIT
ARG DOCKERFILE_PATH
ARG SOURCE_TYPE

ENV DEBIAN_FRONTEND=noninteractive LANG=en_US.UTF-8 LC_ALL=C.UTF-8 LANGUAGE=en_US.UTF-8 TERM=dumb DBUS_SESSION_BUS_ADDRESS=/dev/null \
    FIREFOX_VERSION=58.0.2 GECKODRIVER_VERSION=0.19.1 \
    SCREEN_WIDTH=1360 SCREEN_HEIGHT=1020 SCREEN_DEPTH=24

RUN rm -rf /var/lib/apt/lists/* && apt-get -q update &&\
  apt-get install -qy --force-yes fontconfig bzip2 curl git xvfb \
    libgtk-3-0 libdbus-1-3 libdbus-glib-1-2 \
    libxss1 libappindicator1 libindicator7 libpango1.0-0 fonts-liberation xdg-utils gconf-service \
    python3-pip libssl-dev libffi-dev libjpeg-dev libpng-dev libwebp-dev libfreetype6-dev libtiff-dev zlib1g-dev \
    libgeos-c1v5 libgeoip1 libgdal20 \
  &&\
  apt-get clean &&\
  rm -rf /var/lib/apt/lists/* &&\
  rm -rf /tmp/*


RUN curl --silent --show-error --location --fail --retry 3 http://ftp.mozilla.org/pub/mozilla.org/firefox/releases/${FIREFOX_VERSION}/linux-x86_64/en-US/firefox-${FIREFOX_VERSION}.tar.bz2 > /tmp/firefox-${FIREFOX_VERSION}.tar.bz2 && mkdir /opt/firefox-${FIREFOX_VERSION} && tar xjf /tmp/firefox-${FIREFOX_VERSION}.tar.bz2 -C /opt/firefox-${FIREFOX_VERSION} && rm /tmp/firefox-${FIREFOX_VERSION}.tar.bz2

RUN curl --silent --show-error --location --fail --retry 3 https://github.com/mozilla/geckodriver/releases/download/v${GECKODRIVER_VERSION}/geckodriver-v${GECKODRIVER_VERSION}-linux64.tar.gz > /tmp/geckodriver-${GECKODRIVER_VERSION}.tar.gz && mkdir /opt/geckodriver-${GECKODRIVER_VERSION} && tar xzf /tmp/geckodriver-${GECKODRIVER_VERSION}.tar.gz -C /opt/geckodriver-${GECKODRIVER_VERSION} && rm /tmp/geckodriver-${GECKODRIVER_VERSION}.tar.gz

RUN ln -s /opt/geckodriver-${GECKODRIVER_VERSION}/geckodriver /usr/bin/geckodriver && chmod +x /usr/bin/geckodriver && \
    ln -s /opt/firefox-${FIREFOX_VERSION}/firefox/firefox /usr/bin/firefox && chmod +x /usr/bin/firefox

LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.docker.dockerfile="$DOCKERFILE_PATH/Dockerfile" \
      org.label-schema.license="GPLv2" \
      org.label-schema.name="Debian stretch build image with Firefox & Python 3.5 running functional tests. Firefox ${FIREFOX_VERSION} and geckodriver ${GECKODRIVER_VERSION}." \
      org.label-schema.url="https://bitbucket.org/nuajik/bitbucket-pipeline-python3-firefox" \
      org.label-schema.vcs-ref=$SOURCE_COMMIT \
      org.label-schema.vcs-type="$SOURCE_TYPE" \
      org.label-schema.vcs-url="https://bitbucket.org/nuajik/bitbucket-pipeline-python3-firefox"
